package logger

// This is the logger from Buildkite's Agent minus the stuff I didn't need
// https://github.com/buildkite/agent/blob/master/logger/log.go

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"golang.org/x/crypto/ssh/terminal"
)

type Level int

const (
	INFO Level = iota
	NOTICE
	DEBUG
	ERROR
	WARN
	FATAL
)

var levelNames = []string{
	"INFO",
	"NOTICE",
	"DEBUG",
	"ERROR",
	"WARN",
	"FATAL",
}

// String returns the string representation of the level
func (l Level) String() string {
	return levelNames[l]
}

const (
	nocolor = "0"
	red     = "31"
	green   = "1;32"
	yellow  = "33"
	blue    = "34"
	gray    = "1;30"
	cyan    = "1;36"
)

var level = INFO
var mutex = sync.Mutex{}
var colors = true

func ColorsEnabled() bool {
	// Colors can only be shown if STDOUT is a terminal
	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		return colors
	} else {
		return false
	}
}

func SetLevel(l Level) {
	level = l

	if level == DEBUG {
		Debug("Debug mode enabled")
	}
}

func Debug(format string, v ...interface{}) {
	if level == DEBUG {
		log(DEBUG, format, v...)
	}
}

func Error(format string, v ...interface{}) {
	log(ERROR, format, v...)
}

func Fatal(format string, v ...interface{}) {
	log(FATAL, format, v...)
	os.Exit(1)
}

func Notice(format string, v ...interface{}) {
	log(NOTICE, format, v...)
}

func Info(format string, v ...interface{}) {
	log(INFO, format, v...)
}

func Warn(format string, v ...interface{}) {
	log(WARN, format, v...)
}

func log(l Level, format string, v ...interface{}) {
	level := strings.ToUpper(l.String())
	message := fmt.Sprintf(format, v...)
	now := time.Now().Format("2006-01-02 15:04:05")
	line := ""

	if ColorsEnabled() {
		prefixColor := green
		messageColor := nocolor

		if l == DEBUG {
			prefixColor = gray
			messageColor = gray
		} else if l == NOTICE {
			prefixColor = cyan
		} else if l == WARN {
			prefixColor = yellow
		} else if l == ERROR {
			prefixColor = red
		} else if l == FATAL {
			prefixColor = red
			messageColor = red
		}

		line = fmt.Sprintf("\x1b[%sm%s %-6s\x1b[0m \x1b[%sm%s\x1b[0m\n", prefixColor, now, level, messageColor, message)
	} else {
		line = fmt.Sprintf("%s %-6s %s\n", now, level, message)
	}

	mutex.Lock()
	fmt.Fprint(os.Stdout, line)
	mutex.Unlock()
}
